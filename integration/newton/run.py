from numpy import linspace as irange
from lagrange import interpolate
from polynom import Polynom


def trivial_integrate(polynom, range):
    range = list(range)
    x0 = range[0]
    integral = 0
    for x in range:
        integral += polynom(x) * (x - x0)
        x0 = x
    return integral


def simpson_integrate(polynom, range_):
    f = polynom
    x = range_
    n = len(x)

    a = x[0]
    b = x[-1]
    h = (b - a) / n
    for i in x:
        print(f'f({i}) = {f(i)}')
    print('-'*80)
    print(f'I = {h}/3({f(a)} + 4*{[str(f(x[2*i - 1])) for i in range(1, n//2)]} + \'+\' for ')
    even = 4*sum((f(x[2*i - 1]) for i in range(1, n//2)))
    odd = 2*sum((f(x[2*i]) for i in range(1, n//2 - 1)))
    return (h/3) * (f(a) + even + odd + f(b))


def main():
    poly = Polynom([0.2, 25, -200, 675, -900, 400])
    print(poly)
    print(trivial_integrate(poly, irange(0, 0.8, 5)))
    print(simpson_integrate(poly, irange(0, 0.8, 5)))


if __name__ == '__main__':
    main()
