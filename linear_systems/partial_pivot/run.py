import gauss as gauss
import numpy as np

def try_for(m, c):
    result, pivoted = gauss.solve(m, c)


    print('\n\n'
          'original:\n'
          '{}\n\n'
          'pivoted:\n'
          '{}\n'
          'result:\n'
          '{}\n'.format(np.concatenate((m, c.T), axis=1), pivoted, result))

try_for(m = np.matrix([[-0.421, 0.784,  0.279],
                       [ 0.448, 0.832,  0.193],
                       [ 0.421, 0.784, -0.207]]),
        c = np.matrix([[0, 1, 0]]))

try_for(m = np.matrix([[ 1., -1., 1.],
                       [ 2., 3., -1.],
                       [-3., 1., 1.]]),
        c = np.matrix([[1, 4, -1]]))
