import numpy as np
from enum import Enum

class Axis(Enum):
    ROWS = 0
    COLUMNS = 1

def swap(matrix, i, j, axis=Axis.ROWS):
    if axis == Axis.ROWS:
        t = np.copy(matrix[i, :])
        matrix[i, :] = matrix[j, :]
        matrix[j, :] = t
    elif axis == Axis.COLUMNS:
        t = np.copy(matrix[:, i])
        matrix[:, i] = matrix[:, j]
        matrix[:, j] = t

def pivote(matrix):
    height, width = matrix.shape
    for i in range(height):
        pivot = np.argmax(abs(matrix[i:, i])) + i
        swap(matrix, i, pivot)
        for j in range(i + 1, height):
            ratio = matrix[j, i] / matrix[i, i]
            matrix[j] -= ratio * matrix[i]


def pivoted(matrix):
    matrix = matrix.copy()
    pivote(matrix)
    return matrix


def retrosubstitution(matrix):
    height, _ = matrix.shape
    solution = [0] * height
    for i in reversed(range(height)):
        solution[i] = (matrix[i, height] - sum(matrix[i, j] * solution[j]
                                               for j in range(i, height))
                       )/matrix[i, i]
    return solution


def solve(matrix, coeficients):
    matrix = np.concatenate((matrix, coeficients.T), axis=1)
    pivoted_ = pivoted(matrix)
    return retrosubstitution(pivoted_), pivoted_

