import numpy as np
from extra import print_matrix


def empty(shape):
    return np.matrix(np.zeros(shape))


def identity(shape):
    m = empty(shape)
    for i in range(shape[0]):
        m[i, i] = 1
    return m


def decompose(matrix):
    height, width = matrix.shape
    l, u = empty(matrix.shape), identity(matrix.shape)
    l[:, 0] = matrix[:, 0]
    u[0] = matrix[0] / l[0, 0]
    for k in range(1, height):
        for i in range(k, height):
            l[i, k] = matrix[i, k] - sum((l[i, r]*u[r, k] for r in range(k-1)))
        for j in range(k + 1, width):
            u[k, j] = (matrix[k, j] - sum((l[k, r]*u[r, j] for r in range(k-1)))) / l[k, k]

    return l, u


def ly_b(l, u, b):
    n = l.shape[0]
    y = [0] * n
    for i in range(n):
        y[i] = (b[i] - sum((l[i, j] * y[j] for j in range(i-1)))) / l[i, i]
    return y


def ux_y(l, u, y):
    n = l.shape[0]
    x = [0] * n
    for i in reversed(range(n)):
        x[i] = (y[i] - sum((y[i, j] * x[j] for j in reversed(range(i-1)))))
    return x


def solve(matrix, coeficients):
    l, u = decompose(np.concatenate((matrix, coeficients.T), axis=1))
    solution = ux_y(l, u, ly_b(l, u, coeficients))
    return solution
