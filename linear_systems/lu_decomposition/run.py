import numpy as np
from lu import solve
from extra import print_matrix

m = np.matrix([
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
    ])
c = np.matrix([10, 11, 12])

l, u = solve(m, c)

print_matrix('M', m)
print_matrix('C', c)
print()
print_matrix('L', l)
print_matrix('U', u)
