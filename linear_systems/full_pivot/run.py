import gauss
import numpy as np
m = np.matrix([
        [-0.421, 0.784, 0.279],
        [0.448, 0.832, 0.193],
        [0.421, 0.784, 0.207]
    ])
c = np.matrix([[0, 1, 0]])
result = gauss.solve(m, c)

print('\n\n'
      'original:\n'
      '{}\n'
      'result:\n'
      '{}'.format(m, result))
