import numpy as np
from extra import print_matrix
from enum import Enum

class Axis(Enum):
    ROWS = 0
    COLUMNS = 1

def swap(matrix, i, j, axis=Axis.ROWS):
    if i == j:
        return
    if axis == Axis.ROWS:
        t = np.copy(matrix[i, j:])
        matrix[i, j:] = matrix[j, j:]
        matrix[j, j:] = t
    elif axis == Axis.COLUMNS:
        t = np.copy(matrix[j:, i])
        matrix[j:, i] = matrix[j:, j]
        matrix[j:, j] = t


def max_element(matrix, i):
    height, width = matrix.shape

    pivot = (i, i)
    max_ = matrix[i, i]
    for j, row in enumerate(matrix):
        for k, col in enumerate(row):
            if (col > max_).all():
                pivot = (j, k)
                max_ = col

    return pivot


def full_pivoting(matrix):
    height, width = matrix.shape
    for i in range(height):
        pivot = max_element(matrix, i)
        swap(matrix, i, pivot[0], axis=Axis.COLUMNS)
        swap(matrix, i, pivot[1], axis=Axis.ROWS)
        for j in range(i + 1, height):
            ratio = matrix[j, i] / matrix[i, i]
            matrix[j] -= ratio * matrix[i]


def solve(matrix, coeficients):
    matrix_ = np.concatenate((matrix, coeficients.T), axis=1)
    full_pivoting(matrix_)
    return matrix_
