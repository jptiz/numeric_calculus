import numpy as np
from enum import Enum
from gauss import swap

def partial_pivoting(matrix):
    height, width = matrix.shape
    lines = [i for i in range(height)]
    for i in range(height):
        pivot = np.argmax(abs(matrix[lines[i]:, i])) + i
        lines[i], lines[pivot] = lines[pivot], lines[i]
        for j in range(i + 1, height):
            ratio = matrix[lines[j], i] / matrix[lines[i], i]
            matrix[lines[j]] -= ratio * matrix[lines[i]]
    for i in range(height):
        if lines[i] is None:
            continue
        swap(matrix, i, lines[i])
        lines[lines[i]] = None
        lines[i] = None


def solve(matrix, coeficients):
    matrix_ = np.concatenate((matrix, coeficients.T), axis=1)
    partial_pivoting(matrix_)
    return matrix_

