from textwrap import indent

def print_matrix(name, m):
    print('{}:'.format(name))
    print(indent('{}'.format(m), '  '))
