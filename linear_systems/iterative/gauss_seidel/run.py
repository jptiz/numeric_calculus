import numpy as np
from seidel import solve
from extra import print_matrix

m = np.matrix([
        [3,-1,-1],
        [1, 3, 1],
        [2,-2, 4],
    ])
c = np.matrix([1, 5, 4])

solved = solve(m, c)

print_matrix('M', m)
print_matrix('C', c)
print()
print_matrix('S', solved)
