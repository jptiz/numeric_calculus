import numpy as np

def solve(matrix, coeficients):
    height, width = matrix.shape

    epsilon = 0.1**10
    x = np.matrix([0] * width)
    new_x = np.matrix([epsilon] * width)

    it = 0
    max_it = 10000
    while it < max_it and (max(abs(new_x - x)) >= epsilon).any():
        x = new_x.copy()
        for i in range(width):
            row_sum = sum((matrix[i, j] * x[0, j]) for j in range(width) if i != j)
            new_x[0, i] = (coeficients[0, i] - row_sum) / matrix[i, i]
        it += 1

    if it >= max_it:
        print('{} iterations reached. Stopping...'.format(max_it))
    else:
        print('Done ({} iterations)'.format(it))

    return x
