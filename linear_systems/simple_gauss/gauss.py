import numpy as np

def triangulate(matrix):
    width, height = matrix_.shape
    for i in range(0, width):
        for j in range(i + 1, height):
            ratio = matrix[j][i] / matrix[j][0]
            for k in range(i + 1, width):
                matrix[j][k] = matrix[j][k] - ratio * matrix[0][k]


def solve(matrix, coeficients):
    matrix_ = np.concatenate((matrix, coeficients.T), axis=1)
    triangulate(matrix_)
    return matrix_
