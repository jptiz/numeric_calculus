#!/usr/bin/python3

#-------------------------------------------------------------------------------
# Change to Python3 if using Python2.
#-------------------------------------------------------------------------------
import sys

if sys.version_info.major == 2:
    print("You are currently using Python2. Switching to a true python...")
    from subprocess import call
    call(['python3', __file__])
    exit()

print('Currently using Python3. Good boy.')

#-------------------------------------------------------------------------------
# The code itself
#-------------------------------------------------------------------------------
import numpy as np
import textwrap
from math import isnan

np.seterr(all='raise')

class UnsolvableSystemError(Exception):
    '''
        Thrown when a system has no solution (for example: 0x + 0y = 1).
    '''
    pass

#-------------------------------------------------------------------------------
# Solution itself
#-------------------------------------------------------------------------------
def thomas_solve(m, c):
    '''
        Solves system using Thomas' Algorithm.
    '''
    for i, line in enumerate(m):
        if ((abs(line)).sum() + abs(c[0, i]) == 0).any():
            raise UnsolvableSystemError(
                    'm cannot have a zero-filled line with non-zero value in corresponding column c.'
                )
    m = m.copy()
    c = c.copy()[0]
    
    n = len(m)
    
    c_ = [0] * n
    for i in range(1, n-1):
        a, b, c__ = m[i, i - 1], m[i, i], m[i, i + 1]
        try:
            c_[i] = c__ / (b - c_[i - 1] * a)
        except FloatingPointError:
            raise UnsolvableSystemError('Division by zero occurred while solving system.')
        
    d_ = [0] * n
    for i in range(1, n):
        a, b = m[i, i - 1], m[i, i]
        try:
            d_[i] = (c[i] - d_[i - 1] * a) / (b - c_[i - 1] * a)
        except FloatingPointError:
            raise UnsolvableSystemError('Division by zero occurred while solving system.')
        
    x = [0] * n
    x[n - 1] = d_[n - 1]
    for i in reversed(range(n - 1)):
        x[i] = d_[i] - c_[i] * x[i + 1]
        
    return x


#-------------------------------------------------------------------------------
# Tools
#-------------------------------------------------------------------------------
def print_matrix(name, m):
    '''
        Prints matrix `m` idented and shows its `name`.
        
        args:
            name(str): Matrix name.
            m(np.matrix): The matrix.
    '''
    print('{}:'.format(name))
    print(textwrap.indent('{}'.format(m), '  '))
    
    
def random_tridiagonal_matrix(n, low=-5, high=5):
    '''
        Generates a random tridiagonal matrix with values from `low` to `high`.
    '''
    matrix = np.tri(n, k=1) * (1 - np.tri(n, k=-2))
    random_matrix = np.random.randint(low=low, high=high, size=(n, n))
    return np.matrix(matrix * random_matrix)


def random_system(size=10):
    '''
        Generates a random tridiagonal system with given `size`.
    '''
    low, high = -5, 5
    m = random_tridiagonal_matrix(size, low, high)
    c = np.random.randint(low=low, high=high, size=(1, size))
    return m, c


if __name__ == '__main__':
    m, c = random_system(10)
    print('== before solution ==')
    print_matrix('M', m)
    print_matrix('C', c)
    try:
        x = thomas_solve(m, c)
        print('== after solution ==')
        print('solution:')
        for i, x_ in enumerate(x):
            print('\tx{} = {},'.format(i, x_))
        
        print_matrix('m * x', m * np.matrix(x).reshape((10, 1)))
    except UnsolvableSystemError as e:
        print('System is not solvable: {}'.format(e))
	 	  	 	  	      	  	      	   	     	       	 	
