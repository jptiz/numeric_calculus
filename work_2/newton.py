from functools import reduce
from operator import mul

from polynom import Polynom


class NewtonPolynom(Polynom):
    def x(self, index):
        return self.polynom[index][1]

    def __call__(self, x):
        return sum((a * (reduce(mul, (x - self.x(j) for j in range(i))) if i > 0 else 1)
               for i, (a, _) in enumerate(self.polynom)))


def interpolate(points):
    lookup = dict()

    def f(s, e):
        if not (s, e) in lookup:
            if s == e:
                return points[s][1]
            left = f(s+1, e)
            right = f(s, e-1)
            lookup[(s, e)] = (left - right) / (points[e][0] - points[s][0])
        return lookup[(s, e)]

    return NewtonPolynom([(f(0, e), points[e][0]) for e in range(len(points))])
