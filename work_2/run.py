#!/usr/bin/python3

#-------------------------------------------------------------------------------
# Change to Python3 if using Python2.
#-------------------------------------------------------------------------------
import sys

if sys.version_info.major == 2:
    print("You are currently using Python2. Switching to a Python3...")
    from subprocess import call
    call(['python3', __file__])
    exit()

print('Currently using Python3.')

PLOT = True

import numpy
from numpy.linalg import solve

try:
    import matplotlib
    matplotlib.use('Qt5Agg')
    from matplotlib import pyplot
except ImportError as e:
    print(f'Failed to import matplotlib: {e.msg}\nNo plottings shall be done.')
    PLOT = False

from extra import print_matrix
from polynom import Polynom

import vandermonde
import lagrange
import newton


numpy.set_printoptions(linewidth=120, precision=6)


def show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False)], title='Polynom view'):
    if not PLOT:
        return
    pyplot.figure(1)
    pyplot.subplot(111)

    xs = x
    for style, x, highlight in xs:
        y = [polynom(x_) for x_ in x]

        pyplot.plot(x, y, style)
        if highlight:
            axis = pyplot.figure(1).add_subplot(111)
            for x_, y_ in zip(x, y):
                axis.annotate(f'{y_:.2f}', xy=(x_ + 1, y_ + .1), textcoords='data')

    pyplot.ylim(5, 16)
    pyplot.grid(True)
    pyplot.title(title)
    pyplot.show()


def solve_by(f, data, method):
    polynom = Polynom(solve(*method.interpolate(f, data)))
    print_matrix('polynom', polynom)

    show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False),
                             ('bo', list(data), True),
                             ('ro', [105], True),
                             ], title='Question 1')
    x = 105
    print(f'P({x}) = {polynom(x)}')


def question_1(data):
    def f(x):
        return data[x]

    solve_by(f, data, vandermonde)


def question_2(data):
    polynom = lagrange.interpolate(list(data.items()))
    x = 105
    print(f'P({x}) = {polynom(x)}')

    show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False),
                             ('bo', list(data), True),
                             ('ro', [105], True),
                             ], title='Question 2 (Lagrange)')


def question_3(data):
    polynom = newton.interpolate(list(data.items()))
    print(f'polynom: {polynom}')
    x = 105
    print(f'P({x}) = {polynom(x)}')

    show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False),
                             ('bo', list(data), True),
                             ('ro', [105], True),
                             ], title='Question 3 (Newton Divided Differences)')


def question_4(data):
    pass


def run_question(n, data):
    print( '===========================\n'
          f'= Question {n}              =\n'
           '===========================')
    exec(f'question_{n}(data)')


if __name__ == '__main__':
    data = {55: 14.08,
            70: 13.56,
            85: 13.28,
            100: 12.27,
            120: 11.30,
            140: 10.4,}
    for i in range(1, 5):
        run_question(i, data)
