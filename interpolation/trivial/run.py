#!/usr/bin/python3
import numpy
from numpy import (log as ln, array)
from vandermonde import interpolate

from gauss import solve, retrosubstitution, pivoted
from polynom import Polynom

from extra import print_matrix, irange

numpy.set_printoptions(linewidth=120, precision=6)


def compare_to_original(m, solution):
    print('what if...: {}'.format(numpy.dot(m, [-1.1234, 1.3048, -0.1975, -0.0004112])))
    print('what if (vina style)...: {}'.format(numpy.dot(m, [-1.127, 1.31, -0.2, -5.68989300e-13])))

def show_solution(solution, pivoted):
    print_matrix('solution', array(solution))
    print_matrix('m * solution', numpy.dot(m, solution))
    print('is close to original?: {}'.format(numpy.allclose(numpy.dot(m, solution), y)))

if __name__ == '__main__':
    m, y = interpolate(ln, range_=irange(2, 2.15, 0.05))
    y = list(y)

    print_matrix('m', m)
    print_matrix('y', array(y).reshape(len(y), 1))

    matrix = numpy.concatenate((m, array(y).reshape(len(y), 1)), axis=1)
    print_matrix('full matrix', matrix)

    solution, pivoted = solve(m, array(y).reshape(1, len(y)))
    show_solution(solution, pivoted)

    compare_to_original(m, solution)

    print()
    polynom = Polynom(solution)

    print('comparting to ln(x):\n'
          '\t     x   |   ln(x)  |    P(x)  \n'
          '\t---------|----------|----------')
    for x in irange(2, 2.15, 0.05):
        print('\t{x: >#8.3f} | {lnx: >#8.3f} | {px: >#8.3f}'.format(x=x, lnx=ln(x), px=polynom(x)))
