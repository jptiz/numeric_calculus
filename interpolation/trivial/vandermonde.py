#!/usr/bin/python3
from numpy import array, vectorize


def interpolate(f, range_):
    '''Interpolates function with values defined in range.
    
    Args:
        f(function): The function.
        range_(irange): The range of values.
        
    Returns: Tuple containing the Vandermonde matrix and the Y matrix.'''

    range_ = list(range_)
    vanvan_matrix = array([[x**i for i, _ in enumerate(range_)]
                                 for x in range_])
    y_matrix = vectorize(f)
    return vanvan_matrix, y_matrix(range_)
