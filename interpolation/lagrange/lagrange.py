from functools import reduce
from operator import mul

from polynom import Polynom


def L(p, x, j):
    res = reduce(mul, [((x - p[i][0])/(p[j][0] - p[i][0])) for i in range(len(p)) if i != j])
    return res


class LagrangePolynom(Polynom):
    def __call__(self, x):
        return sum(((y * L(self.polynom, x, j) for j, (_, y) in enumerate(self.polynom))))


def interpolate(points):
    return LagrangePolynom(points)
