from extra import irange


class Polynom:
    def __init__(self, polynom):
        self.polynom = polynom

    def __call__(self, x):
        result = 0
        for i, c in enumerate(self.polynom):
            result += c * (x ** i)
        return result

    def __str__(self):
        return str(self.polynom)

    def __getitem__(self, index):
        return self.polynom[index]

    def __setitem__(self, index, value):
        self.polynom[index] = value


if __name__ == '__main__':
    p = Polynom([1, 2])
    for i in irange(-2, 2, 0.48):
        print('p({:5.2f}) = {:5.2f}'.format(i, p(i)))
