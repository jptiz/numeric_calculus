#!/usr/bin/python3
import math
from bisection import find_root
from extra import print_functions, try_for


def f(x):
    return 9*x**3 + 2*x - 3


def exsenxm1(x):
    return math.exp(x)*math.sin(x) - 1


print_functions([f, exsenxm1])

try_for(find_root, f)
try_for(find_root, exsenxm1, a=0, b=1)
