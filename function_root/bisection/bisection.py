def find_root(f, a=-10**6, b=10**6, epsilon=2**-100, steps=0):
    steps += 1
    if f(a) == 0:
        return a
    elif f(b) == 0:
        return b
    m = (a + b) / 2
    if abs(f(m)) < epsilon or abs(b - a) < epsilon:
        return m, steps
    if f(a) * f(m) < 0:
        return find_root(f, a, m, epsilon, steps)
    return find_root(f, m, b, epsilon, steps)
