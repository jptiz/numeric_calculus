#!/usr/bin/python3
import math
from extra import try_for
from birge import find_root
from polynom import Polynom

try_for(find_root, polynom=Polynom([1, 0, 2, -1]), guess=1, it_limit=3, epsilon=10**-50)
