def find_root(polynom, guess=1, it_limit=10**8, epsilon=2**-300):
    x = guess

    for i in range(it_limit):
        b = [polynom[0]]
        print(f'b[0] = {b[0]}')
        for j, a in enumerate(polynom[1:]):
            b.append(a + b[j] * x)
            print(f'b[{j+1}] = a + b{j} * x = {a} + {b[j]:2.2f} * {x:2.2f} = {b[-1]:2.2f}')

        print()
        c = [b[0]]
        print(f'c[0] = {c[0]}')
        for j, b_ in enumerate(b[1:-1]):
            c.append(b_ + c[j] * x)
            print(f'c[{j+1}] = b_ + c{j} * x = {b_:2.2f} + {c[j]:2.2f} * {x:2.2f} = {c[-1]:2.2f}')

        print('-'*80)

        x_ = x - b[-1] / c[-1]

        if abs(polynom(x_) - polynom(x)) <= epsilon:
            return x_, i + 1
        x = x_
    return x_, i + 1
