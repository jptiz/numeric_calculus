def find_root(f, guess=1, it_limit=10**2, epsilon=10**-15):
    # x_ = x_{k - 1}
    # x = x_{k}
    x_ = guess
    x = x_ + epsilon
    last_f = f(x_)

    for i in range(it_limit):
        x_, x = x, x - (x - x_) * f(x) / (f(x) - f(x_))

        if abs(f(x) - last_f) <= epsilon:
            return x, i+1
        last_f = f(x)
    return x, i+1
