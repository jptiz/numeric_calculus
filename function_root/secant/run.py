#!/usr/bin/python3
import math
from extra import try_for
from secant import find_root


def inverse_sqrt5(x):
    return x*x - 5


try_for(find_root, f=inverse_sqrt5, guess=2)
