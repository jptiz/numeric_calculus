#!/usr/bin/python3
import math
from false_position import find_root
from extra import print_functions


def a(x):
    return math.exp(x) + x


def b(x):
    return math.exp(x) - 2 * math.cos(x)


def f(x):
    return 9*x**3 + 2*x - 3


def exsenxm1(x):
    return math.exp(x)*math.sin(x) - 1


print_functions([a, b, f, exsenxm1])

print('------------------- for a(x) in [{}, {}]:'.format(-1, 0))
root = find_root(a, a=-1, b=0, epsilon=10**-2)
print('a({}) = {}'.format(root, a(root)))

print('------------------- for b(x) in [{}, {}]:'.format(0, 2))
root = find_root(b, a=-0, b=2, epsilon=10**-15)
print('b({}) = {}'.format(root, b(root)))

print('------------------- for f(x) in [{}, {}]:'.format(0, 1))
root = find_root(f, a=0, b=1)
print('f({}) = {}'.format(root, f(root)))

print('------------------- for exsenxm1(x) in [{}, {}]:'.format(0, 1))
root = find_root(exsenxm1, a=0, b=1)
print('exsenxm1({}) = {}'.format(root, f(root)))
