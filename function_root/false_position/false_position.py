def find_root(f, a=-10**6, b=10**6, epsilon=2**-300):
    upper = b - a
    lower = f(b) - f(a)
    k = a - f(a) * upper / lower

    if abs(f(k)) < epsilon or abs(b - a) < epsilon:
        return k
    if f(a) * f(k) < 0:
        return find_root(f, a, k, epsilon)
    return find_root(f, k, b, epsilon)
