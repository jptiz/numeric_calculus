#!/usr/bin/python3
import math
from extra import try_for
from newton import find_root


def sqrt(x):
    return x*x - 5

def sqrt_(x):
    return 2*x


try_for(find_root, f=sqrt, f_=sqrt_, guess=2, epsilon=10**-50)
