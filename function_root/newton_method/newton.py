def find_root(f, f_, guess=1, it_limit=10**8, epsilon=2**-300):
    x = guess

    for i in range(it_limit):
        x_ = x - f(x) / f_(x)

        if abs(f(x_) - f(x)) <= epsilon:
            return x_, i + 1
        x = x_
    return x_, i + 1
