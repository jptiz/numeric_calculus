import inspect
import numpy
from numpy import arange
from textwrap import indent


def irange(start, stop, step):
    for i in arange(start, stop + step, step):
        yield i


def print_matrix(name, m, precision=5):
    numpy.set_printoptions(linewidth=120, precision=precision)
    print('{}:'.format(name))
    print(indent('{}'.format(m), '  '))


def print_functions(functions):
    for function in functions:
        for line in inspect.getsourcelines(function)[0]:
            print(line, end='')
        print()
    print()


def try_for(method, *args, **kwargs):
    try:
        f = kwargs['f']
        print('---------------------------------------------------------------------------------')
        print('** \t for {}(x):'.format(f.__name__))
        root, steps = method(**kwargs)
        print('** \t   {function}({arg}) = {res:.30f} \t\t [with {steps} steps]'.format(
                function=f.__name__,
                arg=root,
                res=f(root),
                steps=steps
            )
        )
    except KeyError:
        p = kwargs['polynom']
        print('---------------------------------------------------------------------------------')
        print(f'** \t for {p}:')
        root, steps = method(**kwargs)
        print('** \t   p({arg}) = {res:.30f} \t\t [with {steps} steps]'.format(
                arg=root,
                res=p(root),
                steps=steps
            )
        )

