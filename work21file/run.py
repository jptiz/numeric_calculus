#!/usr/bin/python3

#-------------------------------------------------------------------------------
# Change to Python3 if using Python2.
#-------------------------------------------------------------------------------
import sys

if sys.version_info.major == 2:
    print("You are currently using Python2. Switching to a Python3...")
    from subprocess import call
    call(['python3', __file__])
    exit()

print('Currently using Python3.')

PLOT = False

import numpy
from numpy.linalg import solve

try:
    import matplotlib
    matplotlib.use('Qt5Agg')
    from matplotlib import pyplot
except ImportError as e:
    print(f'Failed to import matplotlib: {e.msg}\nNo plottings shall be done.')
    PLOT = False

from polynom import Polynom

#------------------------------------------------------------------------------
# Extra stuff
#------------------------------------------------------------------------------
import inspect
from numpy import arange
from textwrap import indent


def irange(start, stop, step):
    for i in arange(start, stop + step, step):
        yield i


def print_matrix(name, m, precision=5):
    numpy.set_printoptions(linewidth=120, precision=precision)
    print('{}:'.format(name))
    print(indent('{}'.format(m), '  '))


#------------------------------------------------------------------------------
# Vandermonde
#------------------------------------------------------------------------------
from numpy import array, vectorize


def vandermonde_interpolate(f, range_):
    '''Interpolates function with values defined in range.

    Args:
        f(function): The function.
        range_(irange): The range of values.

    Returns: Tuple containing the Vandermonde matrix and the Y matrix.'''

    range_ = list(range_)
    vanvan_matrix = array([[x**i for i, _ in enumerate(range_)]
                                 for x in range_])
    y_matrix = vectorize(f)
    return vanvan_matrix, y_matrix(range_)


#------------------------------------------------------------------------------
# Lagrange
#------------------------------------------------------------------------------
from functools import reduce
from operator import mul


def L(p, x, j):
    res = reduce(mul, [((x - p[i][0])/(p[j][0] - p[i][0])) for i in range(len(p)) if i != j])
    return res


class LagrangePolynom(Polynom):
    def __call__(self, x):
        return sum(((y * L(self.polynom, x, j) for j, (_, y) in enumerate(self.polynom))))


def lagrange_interpolate(points):
    return LagrangePolynom(points)


#------------------------------------------------------------------------------
# Newton Divided Differences
#------------------------------------------------------------------------------
class NewtonPolynom(Polynom):
    def x(self, index):
        return self.polynom[index][1]

    def __call__(self, x):
        return sum((a * (reduce(mul, (x - self.x(j) for j in range(i))) if i > 0 else 1)
               for i, (a, _) in enumerate(self.polynom)))


def newton_interpolate(points):
    lookup = dict()

    def f(s, e):
        if not (s, e) in lookup:
            if s == e:
                return points[s][1]
            left = f(s+1, e)
            right = f(s, e-1)
            lookup[(s, e)] = (left - right) / (points[e][0] - points[s][0])
        return lookup[(s, e)]

    return NewtonPolynom([(f(0, e), points[e][0]) for e in range(len(points))])


#------------------------------------------------------------------------------
# The solution itself
#------------------------------------------------------------------------------
numpy.set_printoptions(linewidth=120, precision=6)


def show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False)], title='Polynom view'):
    if not PLOT:
        return
    pyplot.figure(1)
    pyplot.subplot(111)

    xs = x
    for style, x, highlight in xs:
        y = [polynom(x_) for x_ in x]

        pyplot.plot(x, y, style)
        if highlight:
            axis = pyplot.figure(1).add_subplot(111)
            for x_, y_ in zip(x, y):
                axis.annotate(f'{y_:.2f}', xy=(x_ + 1, y_ + .1), textcoords='data')

    pyplot.ylim(5, 16)
    pyplot.grid(True)
    pyplot.title(title)
    pyplot.show()


def question_1(data):
    def f(x):
        return data[x]

    polynom = Polynom(solve(*vandermonde_interpolate(f, data)))
    print_matrix('Polynom (Vandermonde)', polynom)

    show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False),
                             ('bo', list(data), True),
                             ('ro', [105], True),
                             ], title='Question 1')
    x = 105
    print(f'P({x}) = {polynom(x)}')


def question_2(data):
    polynom = lagrange_interpolate(list(data.items()))
    x = 105
    print(f'P({x}) = {polynom(x)}')

    show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False),
                             ('bo', list(data), True),
                             ('ro', [105], True),
                             ], title='Question 2 (Lagrange)')


def question_3(data):
    polynom = newton_interpolate(list(data.items()))
    print_matrix('Polynom (Newton)', [i for (i, x) in polynom.polynom])
    x = 105
    print(f'P({x}) = {polynom(x)}')

    show_graphic(polynom, x=[('b-', numpy.arange(50, 150, .1), False),
                             ('bo', list(data), True),
                             ('ro', [105], True),
                             ], title='Question 3 (Newton Divided Differences)')


def question_4(data):
    print('Sorry, spline requires scipy. VPL doesn\'t have scipy installed :(')


def run_question(n, data):
    print( '===========================\n'
          f'= Question {n}              =\n'
           '===========================')
    exec(f'question_{n}(data)')


if __name__ == '__main__':
    data = {55: 14.08,
            70: 13.56,
            85: 13.28,
            100: 12.27,
            120: 11.30,
            140: 10.4,}
    for i in range(1, 5):
        run_question(i, data)
